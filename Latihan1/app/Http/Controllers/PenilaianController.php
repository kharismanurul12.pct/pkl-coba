<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penilaian;

class PenilaianController extends Controller
{
    public function index(){
        $data = Penilaian::orderBy('id','desc')->get();
        return view('penilaian.index', ['data' => $data]);
    }

    public function create(){
        return view('penilaian.create');
    }

    public function save(Request $request){
        $penilaian = New Penilaian();
        $penilaian->name = $request->name;
        $penilaian->judul = $request->judul;
        $penilaian->pencantuman = $request->pencantuman;
        $penilaian->abstrak = $request->abstrak;
        $penilaian->ktkunci = $request->ktkunci;
        $penilaian->sistematika = $request->sistematika;
        $penilaian->pemanfaatan = $request->pemanfaatan;
        $penilaian->pengacuan = $request->pengacuan;
        $penilaian->dfpustaka = $request->dfpustaka;
        $penilaian->istilah = $request->istilah;
        $penilaian->makna = $request->makna;
        $penilaian->dampak = $request->dampak;
        $penilaian->nisbah = $request->nisbah;
        $penilaian->kemutakhiran = $request->kemutakhiran;
        $penilaian->hasil = $request->hasil;
        $penilaian->penyimpulan = $request->penyimpulan;
        $penilaian->plagiat = $request->plagiat;
        $penilaian->save(); 
        return redirect('/penilaian');
    }

    public function delete($id){
        $penilaian = Penilaian::where('id',$id)->delete();
        return redirect('/penilaian');
    }

    public function edit($id){
        $penilaian = Penilaian::where('id',$id)->first();
        return view('penilaian.edit',['penilaian' => $penilaian]);
    }

    public function update(Request $request){
        $penilaian = Penilaian::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'judul' => $request->judul,
                'pencantuman' => $request->pencantuman,
                'abstrak' => $request->abstrak,
                'ktkunci' => $request->ktkunci,
                'sistematika' => $request->sistematika,
                'pemanfaatan' => $request->pemanfaatan,
                'pengacuan' => $request->pengacuan,
                'dfpustaka' => $request->dfpustaka,
                'istilah' => $request->istilah,
                'makna' => $request->makna,
                'dampak' => $request->dampak,
                'nisbah' => $request->nisbah,
                'kemutakhiran' => $request->kemutakhiran,
                'hasil' => $request->hasil,
                'penyimpulan' => $request->penyimpulan,
                'plagiat' => $request->plagiat,
            ]
        );
        return redirect('/penilaian');
    }
}
