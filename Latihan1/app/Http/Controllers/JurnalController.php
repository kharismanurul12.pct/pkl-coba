<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jurnal;

class JurnalController extends Controller
{
    public function index(){
        $data = Jurnal::get();
        return view('jurnal.index', ['data' => $data]);
    }

    public function create(){
        return view('jurnal.create');
    }

    public function save(Request $request){
        $jurnal = New Jurnal();
        $jurnal->name = $request->name;
        $jurnal->title = $request->title;
        $jurnal->save(); 
        return redirect('/jurnal');
    }

    public function delete($id){
        $jurnal = Jurnal::where('id',$id)->delete();
        return redirect('/jurnal');
    }

    public function edit($id){
        $jurnal = Jurnal::where('id',$id)->first();
        return view('jurnal.edit',['jurnal' => $jurnal]);
    }

    public function update(Request $request){
        $jurnal = Jurnal::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'title' => $request->title,
            ]
        );
        return redirect('/jurnal');
    }
}
