<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penilaian', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longtext('judul');
            $table->longtext('pencantuman');
            $table->longtext('abstrak');
            $table->longtext('ktkunci');
            $table->longtext('sistematika');
            $table->longtext('pemanfaatan');
            $table->longtext('pengacuan');
            $table->longtext('dfpustaka');
            $table->longtext('istilah');
            $table->longtext('makna');
            $table->longtext('dampak');
            $table->longtext('nisbah');
            $table->longtext('kemutakhiran');
            $table->longtext('hasil');
            $table->longtext('penyimpulan');
            $table->longtext('plagiat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penilaian');
    }
};
