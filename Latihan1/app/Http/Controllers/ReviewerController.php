<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reviewer;

class ReviewerController extends Controller
{
    public function index(){
        $data = Editor::orderBy('id','desc')->get();
        return view('editor.index', ['data' => $data]);
    }

    public function create(){
        return view('editor.create');
    }

    public function save(Request $request){
        $editor = New Editor();
        $editor->name = $request->name;
        $editor->bidang = $request->bidang;
        $editor->save(); 
        return redirect('/editor');
    }

    public function delete($id){
        $editor = Editor::where('id',$id)->delete();
        return redirect('/editor');
    }

    public function edit($id){
        $editor = Editor::where('id',$id)->first();
        return view('editor.edit',['editor' => $editor]);
    }

    public function update(Request $request){
        $editor = Editor::where('id',$request->id)->update(
            [
                'name' => $request->name,
                'bidang' => $request->bidang,
            ]
        );
        return redirect('/editor');
    }
}
