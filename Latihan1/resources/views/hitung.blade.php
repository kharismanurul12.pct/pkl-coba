@extends('layouts.app')

@section('title', 'pbkk')
@section('main')

Nilai anda adalah {{ $nilai }}

@if($nilai>=85 AND $nilai<=100)
Grade A
@elseif($nilai>=70 AND $nilai< 85)
    Grade B
@elseif($nilai>=60 AND $nilai< 70)
    Grade C
@elseif($nilai>=50 AND $nilai< 60)
    Grade D
@elseif($nilai>=0 AND $nilai< 50)
    Grade E

@endif
@endsection